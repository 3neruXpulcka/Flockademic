import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function isPeriodicalSlugAvailable(
  database: Database,
  proposedIdentifier: string,
): Promise<boolean> {
  interface Row { uuid: string; }

  const foundSlug = await database.oneOrNone<Row>(
    // tslint:disable-next-line:no-invalid-template-strings
    'SELECT uuid FROM periodicals WHERE date_published IS NOT NULL AND identifier=${slug}',
    { slug: proposedIdentifier },
  );

  return (foundSlug === null);
}
