import { parse } from 'query-string';
import * as React from 'react';
import { Provider } from 'react-redux';
import { applyMiddleware, compose, createStore } from 'redux';
import thunkMiddleware, { ThunkDispatch } from 'redux-thunk';

import { AppShell } from './components/appShell/component';
import {
  abortOrcidVerification,
  AccountState,
  initiateOrcidVerification,
  InitiateOrcidVerificationAction,
} from './ducks/account';
import { AppReducer } from './ducks/app';

// TODO: Only enable the devTools extension in development?
const composeEnhancers =
  (typeof window !== 'undefined' && typeof (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__  !== 'undefined')
  /* istanbul ignore next */
  ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
  : compose;
const store = createStore(AppReducer, composeEnhancers(applyMiddleware(thunkMiddleware)));

  // Since this is heavily interacting with the global scope, testing it is more effort than it's worth:
  /* istanbul ignore next */
if (typeof document !== 'undefined' && document.location) {
  const queryParams = parse(document.location.search);
  const state = store.getState();
  if (!state.account.orcid && !state.account.verifying) {
    if (queryParams.error && queryParams.error_description) {
      store.dispatch(abortOrcidVerification());
    } else {
      const redirectUri = `${document.location.protocol}//${document.location.host}${document.location.pathname}`;
      (store.dispatch as ThunkDispatch<AccountState, {}, InitiateOrcidVerificationAction>)(
        initiateOrcidVerification(redirectUri, queryParams.code),
      );
    }
  }
}

const App = ({ /* istanbul ignore next */ renderIndex = true }) => (
  <Provider store={store}>
    <AppShell renderIndex={renderIndex} />
  </Provider>
);

// This might be required to be a default export so we can pre-render it at build-time.
// It might not though; at the time of enabling this rule, though, I choce not to investigate that
// so it would at least apply to new modules. This was the only module using it.
// tslint:disable-next-line:no-default-export
export default App;
