import * as React from 'react';
import { Link } from 'react-router-dom';

import { ScholarlyArticle } from '../../../../../lib/interfaces/ScholarlyArticle';
import { convertToOrcidWorkId, isOrcidWorkLink } from '../../../../../lib/utils/orcid';

interface Props {
  articles: Array<Partial<ScholarlyArticle>>;
  submissionLink?: string;
}

export const ArticleList = (props: Props) => (
  props.articles.length === 0
    ? null
    : (
      <>
        {renderArticles(props.articles)}
        {renderGhostItem(props.submissionLink)}
      </>
    )
);

function renderArticles(articles: Array<Partial<ScholarlyArticle>>) {
    const list = articles.map((article) => {
      const relationshipProps: { itemProp?: string; } = {};

      if (article.isPartOf) {
        relationshipProps.itemProp = 'hasPart';
      }

      const articleLink = (!article.identifier && article.sameAs && isOrcidWorkLink(article.sameAs))
        ? `/article/${convertToOrcidWorkId(article.sameAs)}`
        : `/article/${article.identifier}`;

      return (
        <article
          className="media"
          {...relationshipProps}
          itemScope={true}
          itemType="https://schema.org/ScholarlyArticle"
          key={`article_${article.identifier}_${article.sameAs}`}
        >
          <div className="media-content">
            <h3 itemProp="name" className="is-size-6">
              <Link
                itemProp="url"
                to={articleLink}
                title="Access the full article for free"
              >
                {article.name || 'Untitled article'}
              </Link>
            </h3>
            <p itemProp="description">
              {article.description}
            </p>
          </div>
        </article>
      );
    });

    return <>{list}</>;
}

function renderGhostItem(submissionLink?: string) {
  if (!submissionLink) {
    return null;
  }

  return (
    <article
      className="media"
      data-test-id="ghostItem"
    >
      <div className="media-content">
        <Link
          to={submissionLink}
          title="Submit a new manuscript"
        >
          Submit an article
        </Link>
      </div>
    </article>
  );
}
