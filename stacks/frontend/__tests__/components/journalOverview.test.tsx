import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

import { ArticleList } from '../../src/components/articleList/component';
import {
  JournalOverview,
} from '../../src/components/journalOverview/component';

const mockProps = {
  periodical: {
    creator: { identifier: 'arbitrary_account_id', name: 'Arbitrary name', sameAs: 'arbitrary_orcid' },
    datePublished: 'Arbitrary date',
    identifier: 'arbitrary_id',
    name: 'Arbitrary name',
  },
  session: {
    account: { identifier: 'arbitrary_account_id' },
    identifier: 'arbitrary_session_id',
  },
  url: 'https://flockademic.com/journal/arbitrary_id',
};

it('should display the journal details when the minimal journal details are loaded', () => {
  const mockPeriodical = {
    identifier: 'some_identifier',
  };
  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(toJson(overview)).toMatchSnapshot();
});

it('should display the journal details when all possble journal details are loaded', () => {
  const mockPeriodical = {
    description: 'Some description',
    headline: 'Some headline',
    identifier: 'some_slug',
    name: 'Some name',
  };
  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(toJson(overview)).toMatchSnapshot();
});

it('should display a header image when configured, covered by a gradient to preserve readability', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    image: 'some-image-url',
  };
  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(overview.find('.hero').prop('style').backgroundImage)
    .toBe('linear-gradient(to right, rgba(0,0,0,.5) 50%, rgba(0,0,0,.2)),url(some-image-url)');
});

it('should display a link to the journal settings if the journal is not public yet', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    datePublished: undefined,
    identifier: 'arbitrary_slug',
  };
  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(overview.find('Link[to="/journal/arbitrary_slug/manage"]')).toExist();
});

it('should display a link to the journal settings if the current user is the journal\'s owner', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    creator: { identifier: 'some_account_id' },
    identifier: 'arbitrary_slug',
  };
  const mockSession = {
    account: { identifier: 'some_account_id' },
    identifier: 'arbitrary_session_id',
  };
  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} session={mockSession} />);

  expect(overview.find('Link[to="/journal/arbitrary_slug/manage"]')).toExist();
});

it('should not display a link to the journal settings if the current user is not the journal\'s owner', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    creator: { identifier: 'some_account_id' },
    identifier: 'arbitrary_slug',
  };
  const mockSession = {
    account: { identifier: 'some_other_account_id' },
    identifier: 'arbitrary_session_id',
  };
  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} session={mockSession} />);

  expect(overview.find('Link[to="/journal/arbitrary_slug/manage"]')).not.toExist();
});

it('should not display a link to the journal settings if account details could not be fetched', () => {
  const accountlessMockProps = {
    ...mockProps,
    periodical: {
      ...mockProps.periodical,
      identifier: 'arbitrary_slug',
    },
    session: undefined,
  };

  const overview = shallow(<JournalOverview {...accountlessMockProps} />);

  expect(overview.find('Link[to="/journal/arbitrary_slug/manage"]')).not.toExist();
});

it('should display an error message when the journal\'s articles could not be loaded', () => {
  const articlelessMockProps = {
    ...mockProps,
    articles: null,
  };

  const overview = shallow(<JournalOverview {...articlelessMockProps} />);

  expect(overview.find('.message.is-danger').last()).toExist();
  expect(overview.find('.message.is-danger').last().text())
    .toMatch('Could not load this journal\'s articles, please try again.');
});

it('should display the suggestion to go to the settings when the journal does not have a name yet', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    name: undefined,
  };

  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(overview.find('.message.is-warning')).toExist();
  expect(overview.find('.message.is-warning').text()).toMatch('Your journal does not have a title yet. Give');
});

it('should display the suggestion to go to the settings when the journal is not public yet', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    datePublished: undefined,
  };

  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(overview.find('.message.is-warning')).toExist();
  expect(overview.find('.message.is-warning').text())
    .toMatch('Your journal is not public yet. You can make it public in');
});

// tslint:disable-next-line:max-line-length
it('should display the suggestion to subscribe to the newsletter when the journal is fully configured (in lieu of actual email updates)', () => {
  const overview = shallow(<JournalOverview {...mockProps} />);

  expect(overview.find('.message.is-info')).toExist();
  expect(overview.find('.message.is-info').text())
    .toMatch('Take advantage of new features as soon as they are introduced:');
});

it('should display a button to submit an article when the journal is public', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    datePublished: 'Arbitrary date',
    identifier: 'arbitrary_slug',
  };

  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(overview.find('Link[to="/journal/arbitrary_slug/submit"]')).toExist();
});

it('should not display a button to submit an article when the journal is not yet public', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    datePublished: undefined,
    identifier: 'arbitrary_slug',
  };

  const overview = shallow(<JournalOverview {...mockProps} periodical={mockPeriodical} />);

  expect(overview.find('Link[to="/journal/arbitrary_slug/submit"]')).not.toExist();
});

it('should note where articles will be listed when a journal is not yet public', () => {
  const mockPeriodical = {
    ...mockProps.periodical,
    datePublished: undefined,
  };

  const overview = shallow(<JournalOverview {...mockProps} articles={[]} periodical={mockPeriodical} />);

  expect(overview.find('[data-test-id="articleList"]')).toExist();
  expect(overview.find('[data-test-id="articleList"] p').text())
    .toBe('Once this journal is public, articles it publishes will be listed here.');
});

it('should present a call to upload manuscripts when no articles have been published yet', () => {
  const overview = shallow(<JournalOverview {...mockProps} articles={[]} />);

  expect(overview.find('[data-test-id="articleList"]')).toExist();
  expect(overview.find('[data-test-id="articleList"] p').text())
    .toMatch('This journal has not published any articles yet.');
});

it('should also list a journal\'s articles', () => {
  const mockArticles = [
    { identifier: 'some_article_id', name: 'Some article', description: 'Some abstract' },
  ];

  const overview = shallow(<JournalOverview {...mockProps} articles={mockArticles} />);

  expect(overview.find(ArticleList)).toExist();
});
